<?php

    function insertBuilder($tabela, $campos, $valores) {
        $query = "insert into $tabela (";
        for ($i = 0; $i < count($campos); ++$i) {
            $query .= $campos[$i];
            if ($i <count($campos)-1) {
                $query.=",";
            }
        }
        $query .= ") values (";
        for ($i = 0; $i < count($valores); ++$i) {
            $query .= "'$valores[$i]'";
            if ($i <count($valores)-1) {
                $query.=",";
            }
        }
        $query .= ")";
        return $query;
    }
    
    function selectBuilder($tabela, $criterio ){
        $query = "select * from $tabela";
        if (isset($criterio)) {
            $query.= " where $criterio";
        }
        return $query;
    }

    function deleteBuilder($tabela, $criterio){
        if(!isset($criterio)){
            die("Critério é obrigatorio para delete");
        }
        $query = "delete from $tabela where $criterio";
        return $query;
    }
    
?>