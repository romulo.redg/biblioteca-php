<?php include 'cabecalho.php'; ?>
            
    <div class="container">    
            
    <h1>Consultar Emprestimos</h1>
		<form>
			<div class="form-group">
				<label for="codigo">Código Livro:</label> 
				<input type="number" id="codigo" name="codigo" class="form-control"> 
			</div>
			<div class="form-group">
				<label for="matricula">Matrícula Aluno:</label> 
				<input type="text" id="matricula" name="matricula"  class="form-control">
			</div>
			<button type="submit" onclick="consultarEmprestimos()" class="btn btn-primary">Consultar</button>
		</form>
		<div id="msg">
		
		</div>         
            <table class="table table-striped" hidden>
			<thead class="thead-dark">
				<tr>
					<th scope="col" class="d-none">ID</th>
					<th scope="col">Código Livro</th>
					<th scope="col">Nome Livro</th>
					<th scope="col">Matrícula Aluno</th>
					<th scope="col">Nome Aluno</th>
                    <th scope="col">Ações</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
			</table>

    </div>

<?php include 'rodape.php'; ?>