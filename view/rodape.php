    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/controller/livroController.js"></script>
    <script type="text/javascript" src="../js/controller/alunoController.js"></script>
    <script type="text/javascript" src="../js/controller/emprestimoController.js"></script>
</body>
</html>
