<?php include 'cabecalho.php';?>
	<div class="container">
		<div class="principal">
			<h1>Adicionar aluno</h1>
			<form onsubmit = "cadastrarAluno()">
				<div class="form-group">
					<label for="matricula">Matrícula:</label> 
					<input class="form-control" type="number" id="matricula" name="matricula" required>
				</div>
				<div class="form-group">
					<label for="nome">Nome:</label> 
					<input class="form-control" type="text" id="nome" name="nome" required>
				</div>
				<div class="form-group">
					<label for="email">Email:</label> 
					<input class="form-control" type="email" id="email" name="email" required> 
				</div>
				<button class="btn btn-primary" type="submit">Cadastrar</button>
			</form>
			<div id="msg">
    
			</div>
		</div>
		

	</div>
<?php include 'rodape.php';?>