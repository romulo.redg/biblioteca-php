<?php include 'cabecalho.php'; ?> 
	<div class="container">
		<div class="principal">
			<h1>Consultar Livro</h1>
			<form>
			<div class="form-group">
				<label for="titulo">Titulo:</label> 
				<input type="text" id="titulo" name="titulo" class="form-control"> 
			</div>
			<div class="form-group">
				<label for="autor">Autor:</label> 
				<input type="text" id="autor" name="autor"  class="form-control">
				</div>
				<button type="submit" onclick="consultarLivros()" class="btn btn-primary">Consultar</button>
			</form>
		</div>
		<div id="msg">
    
		</div>
		<table class="table table-striped" hidden>
			<thead class="thead-dark">
				<tr>
					<th scope="col">Código</th>
					<th scope="col">Título</th>
					<th scope="col">Autor</th>
					<th scope="col">Ações</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
			</table>
	</div>
<?php include 'rodape.php';?>