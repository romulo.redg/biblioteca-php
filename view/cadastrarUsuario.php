<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link href="css/bootstrap.min.css" rel="stylesheet" />
<title>Biblioteca - Cadastrar usuario</title>
</head>
<body>
	<div class="container">
		<div class="principal">
			<h1>Adcionar Usuario</h1>
			<form action="controller/adicionarUsuario.php" method="POST">
				<label for="email">Email:</label> 
				<input type="email" id="email" name="email"> 
				<label for="nome">Nome:</label> 
				<input type="text" id="nome" name="nome">
				<label for="senha">Senha:</label> 
				<input type="password" id="senha" name="senha">
				<button type="submit">Cadastrar</button>
			</form>
		</div>

	</div>

</body>
</html>