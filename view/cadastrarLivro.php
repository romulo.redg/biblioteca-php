<?php include 'cabecalho.php';?>
	<div class="container">
		<div class="principal">
			<h1>Adicionar Livro</h1>
			<form method="POST" onsubmit = "cadastrarLivro()">
				<div class="form-group">
					<label for="titulo">Titulo:</label> 
					<input type="text" id="titulo" name="titulo" class="form-control" required> 
				</div>
				<div class="form-group">
					<label for="autor">Autor:</label> 
					<input type="text" id="autor" name="autor" class="form-control" required>
				</div>
				<button type="submit" class="btn btn-primary">Cadastrar</button>
			</form>
		</div>
		<div id="msg">
		</div>

	</div>
<?php include 'rodape.php';?>