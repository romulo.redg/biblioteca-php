<?php include 'cabecalho.php'; ?>
    <div class="container">     
		<h1>Emprestar Livro</h1>
		<form onsubmit = "cadastrarEmprestimo()">
			<div class="form-group">
				<label for="codigo">Código Livro:</label> 
				<input type="number" id="codigo" name="codigo" class="form-control" required> 
			</div>
			<div class="form-group">
				<label for="matricula">Matrícula Aluno:</label> 
				<input type="text" id="matricula" name="matricula"  class="form-control" required>
			</div>
			<button type="submit" class="btn btn-primary">Cadastrar</button>
		</form>
		<div id="msg">
		
		</div>
	</div>
<?php include 'rodape.php'; ?>