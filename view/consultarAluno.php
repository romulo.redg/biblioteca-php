<?php include 'cabecalho.php'; ?> 
	<div class="container">
		<div class="principal">
			<h1>Consultar Aluno</h1>
			<form>
				<div class="form-group">
					<label for="nome">Nome:</label> 
					<input type="text" id="nome" name="nome" class="form-control"> 
				</div>
				<div class="form-group">
					<label for="email">Email:</label> 
					<input type="text" id="email" name="email"  class="form-control">
				</div>
				<div class="form-group">
					<label for="matricula">Matrícula:</label> 
					<input type="text" id="matricula" name="matricula"  class="form-control">
				</div>
				<button type="submit" onclick="consultarAlunos()" class="btn btn-primary">Consultar</button>
			</form>
		</div>
		<div id="msg">
    
		</div>
		<table class="table table-striped" hidden>
			<thead class="thead-dark">
				<tr>
					<th scope="col" class="d-none">ID</th>
					<th scope="col">Matrícula</th>
					<th scope="col">Nome</th>
					<th scope="col">Email</th>
					<th scope="col">Ações</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
			</table>
	</div>
<?php include 'rodape.php';?>