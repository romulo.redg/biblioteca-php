<?php
    include '../data/queryBuilder.php';
    include '../data/connectionFactory.php';
    
    $email = $_POST["email"];
    $nome = $_POST["nome"];
    $matricula = $_POST["matricula"];
    
    $insertQuery = insertBuilder("tb_aluno", ["email","nome","matricula"], [$email,$nome,$matricula]);
    
    $conexao = getConnection();
    
    if(mysqli_query($conexao, $insertQuery)){
        echo "Dados inseridos com sucesso !";
    } else{
        echo "Erro ao inserir dado no banco de dados";
    }

?>