<?php
    include '../data/queryBuilder.php';
    include '../data/connectionFactory.php';

    $email = $_POST["email"];
    $nome = $_POST["nome"];
    $senha = $_POST["senha"];
    
    $insertQuery = insertBuilder("tb_usuario", ["email","nome","senha"], [$email,$nome,$senha]);
    
    $conexao = getConnection();
    
    if(mysqli_query($conexao, $insertQuery)){
        echo "Dados inseridos com sucesso !";
    } else{
        echo "Erro ao inserir dado no banco de dados";
    }
    
?>