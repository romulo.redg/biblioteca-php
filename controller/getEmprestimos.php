<?php
    include '../data/connectionFactory.php';
    include '../data/queryBuilder.php';
    
    $criterio = getCriterio(); 
    
    $selectQuery = selectBuilder("tb_emprestimo as emp inner join tb_aluno as alun on emp.id_aluno = alun.id inner join tb_livro as liv on emp.id_livro = liv.id", $criterio);
    
    $conexao = getConnection();
    
    $result = mysqli_query($conexao, $selectQuery);
    if(!$result){
        echo "<tr>Nenhum registro encontrado</tr>";
        die;
    }
    if (mysqli_num_rows($result)>0) {
        $return = "";
        while ($linha = mysqli_fetch_array($result)) {         
            $return.= "<tr>";
            $return.= "<td class = 'd-none'>" . utf8_encode($linha[0]) ."</td>";
            $return.= "<td>" . utf8_encode($linha["id_livro"]) ."</td>";
            $return.= "<td>" . utf8_encode($linha["titulo"]) . "</td>";
            $return.= "<td>" . utf8_encode($linha["matricula"]) . "</td>";
            $return.= "<td>" . utf8_encode($linha["nome"]) . "</td>";
            $return.= "<td><button onclick='excluirEmprestimo()'><i class='fas fa-trash-alt'></i><span class='sr-only'>Excluir Livro</span></button></td>";
            $return.= "</tr>";
        }
        echo $return;
    }else{
        echo "<tr>Nenhum registro encontrado</tr>";
    }

    function getCriterio(){
        if(isset($_GET['codigo'])){
            $codigo = $_GET['codigo'];
        }
        if(isset($_GET['matricula'])){
            $matricula = $_GET['matricula'];
        }

        $cont = 0;
            
        $criterio = "";
        if(isset($codigo)){
            if($cont != 0){
                $criterio.= " and ";
            }
            $criterio.="id_livro LIKE '%$codigo%'";
            ++$cont;
        }
        if(isset($matricula)){
            if($cont != 0){
                $criterio.= " and ";
            }
            $criterio.="matricula LIKE '%$matricula%'";
            ++$cont;
        }
        if(strlen($criterio)==0){
            $criterio = null;
        }
        return $criterio;

    }
?>
