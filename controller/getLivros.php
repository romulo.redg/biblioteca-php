<?php
    include '../data/connectionFactory.php';
    include '../data/queryBuilder.php';
    
    $criterio = getCriterio(); 
    
    $selectQuery = selectBuilder("tb_livro", $criterio);
    
    $conexao = getConnection();
    
    $result = mysqli_query($conexao, $selectQuery);

    if(!$result){
        echo "<tr>Nenhum registro encontrado</tr>";
        die;
    }
    
    if (mysqli_num_rows($result)>0) {
        $return = "";
        while ($linha = mysqli_fetch_array($result)) {
            $return.= "<tr>";
            $return.= "<td>" . utf8_encode($linha["id"]) ."</td>";
            $return.= "<td>" . utf8_encode($linha["titulo"]) . "</td>";
            $return.= "<td>" . utf8_encode($linha["autor"]) . "</td>";
            $return.= "<td><button onclick='excluirLivro()'><i class='fas fa-trash-alt'></i><span class='sr-only'>Excluir Livro</span></button></td>";
            $return.= "</tr>";
        }
        echo $return;
    }else{
        echo "<tr>Nenhum registro encontrado</tr>";
    }

    function getCriterio(){
        if(isset($_GET['titulo'])){
            $titulo = $_GET['titulo'];
        }
        if(isset($_GET['autor'])){
            $autor = $_GET['autor'];
        }

        $cont = 0;
            
        $criterio = "";
        if(isset($titulo)){
            if($cont != 0){
                $criterio.= " and ";
            }
            $criterio.="titulo LIKE '%$titulo%'";
            ++$cont;
        }
        if(isset($autor)){
            if($cont != 0){
                $criterio.= " and ";
            }
            $criterio.="autor LIKE '%$autor%'";
            ++$cont;
        }
        
        if(strlen($criterio)==0){
            $criterio = null;
        }
        return $criterio;

    }
?>
