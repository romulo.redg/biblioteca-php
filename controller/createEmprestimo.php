<?php
    include '../data/queryBuilder.php';
    include '../data/connectionFactory.php';
    
    $codigo = $_POST["codigo"];
    $matricula = $_POST["matricula"];

    $conexao = getConnection();

    $queryAluno = selectBuilder("tb_aluno", "matricula = $matricula");

    $aluno = mysqli_query($conexao, $queryAluno);
    if(!$aluno){
        echo "Essa matrícula não pertence a um aluno cadastrado";
        die;
    }
    $linha = mysqli_fetch_array($aluno);

    $idAluno = $linha["id"];

    $insertQuery = insertBuilder("tb_emprestimo", ["id_livro","id_aluno"], [$codigo,$idAluno]);
    
    if(mysqli_query($conexao, $insertQuery)){
        echo "Dados inseridos com sucesso !";
    } else{
        echo "Erro ao inserir dado no banco de dados";
    }

?>