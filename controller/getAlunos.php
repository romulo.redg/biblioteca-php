<?php
    include '../data/connectionFactory.php';
    include '../data/queryBuilder.php';
    
    $criterio = getCriterio(); 
    
    $selectQuery = selectBuilder("tb_aluno", $criterio);
    
    $conexao = getConnection();
    
    $result = mysqli_query($conexao, $selectQuery);
    if(!$result){
        echo "<tr>Nenhum registro encontrado</tr>";
        die;
    }
    if (mysqli_num_rows($result)>0) {
        $return = "";
        while ($linha = mysqli_fetch_array($result)) {
            $return.= "<tr>";
            $return.= "<td class='d-none'>" . utf8_encode($linha["id"]) ."</td>";
            $return.= "<td>" . utf8_encode($linha["matricula"]) . "</td>";
            $return.= "<td>" . utf8_encode($linha["nome"]) . "</td>";
            $return.= "<td>" . utf8_encode($linha["email"]) . "</td>";
            $return.= "<td><button onclick='excluirAluno()'><i class='fas fa-trash-alt'></i><span class='sr-only'>Excluir Livro</span></button></td>";
            $return.= "</tr>";
        }
        echo $return;
    }else{
        echo "<tr>Nenhum registro encontrado</tr>";
    }

    function getCriterio(){
        if(isset($_GET['nome'])){
            $nome = $_GET['nome'];
        }
        if(isset($_GET['email'])){
            $email = $_GET['email'];
        }
        if(isset($_GET['matricula'])){
            $matricula = $_GET['matricula'];
        }

        $cont = 0;
            
        $criterio = "";
        if(isset($nome)){
            if($cont != 0){
                $criterio.= " and ";
            }
            $criterio.="nome LIKE '%$nome%'";
            ++$cont;
        }
        if(isset($email)){
            if($cont != 0){
                $criterio.= " and ";
            }
            $criterio.="email LIKE '%$email%'";
            ++$cont;
        }
        if(isset($matricula)){
            if($cont != 0){
                $criterio.= " and ";
            }
            $criterio.="matricula LIKE '%$matricula%'";
            ++$cont;
        }
        if(strlen($criterio)==0){
            $criterio = null;
        }
        return $criterio;

    }
?>
