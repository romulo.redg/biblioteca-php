<?php
    include '../data/connectionFactory.php';
    include '../data/queryBuilder.php';

    $id = $_GET["id"];
    
    $deleteQuery = deleteBuilder("tb_livro", "id=$id");

    $conexao = getConnection();
    
    if(mysqli_query($conexao, $deleteQuery)){
        echo "Livro deletado com sucesso !";
    }
    else{
       echo "Erro ao inserir dado no banco de dados";
    }
    
    endConnection($conexao);
    
?>