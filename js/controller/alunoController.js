function consultarAlunos() {
    event.preventDefault();
    var nome = document.querySelector('#nome').value;
    var email = document.querySelector('#email').value;
    var matricula = document.querySelector('#matricula').value;
    var page = "http://localhost/biblioteca/controller/getAlunos.php";
    var cont = 0;
    page = page + '?'
    if (nome.length){
        if(cont > 0){
            page = page + '&';
        }else{
            cont++;
        }
        page = page + 'nome=' + nome.replace(/ /g,"+");
    }
    if (email.length){
        if(cont > 0){
            page = page + '&';
        }else{
            cont++;
        }
        page = page + 'email=' + email.replace(/ /g,"+");
    }
    if (matricula.length){
        if(cont > 0){
            page = page + '&';
        }else{
            cont++;
        }
        page = page + 'matricula=' + matricula.replace(/ /g,"+");
    }

    var xhr = new XMLHttpRequest();
    xhr.open("GET",  page );
    xhr.addEventListener("load", function(){
        document.querySelector("tbody").innerHTML = xhr.responseText;
        document.querySelector("table").hidden = false;
        console.log(xhr.responseText);
    });
    xhr.send();
}

function cadastrarAluno(){
    event.preventDefault();
    var nome = document.querySelector('#nome').value;
    var email = document.querySelector('#email').value;
    var matricula = document.querySelector('#matricula').value;
    var param = "nome="+nome+"&matricula="+matricula+"&email="+email;
    var page = "http://localhost/biblioteca/controller/createAluno.php";
    var xhr = new XMLHttpRequest();
    xhr.open("POST",  page );
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.addEventListener("load", function(){
    var msg =  '<div class="alert alert-success alert-dismissible fade show" role="alert">'+
                 xhr.responseText+
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                '<span aria-hidden="true">&times;</span>'+
            '</button>'
        '</div>';
    document.querySelector('#msg').innerHTML = msg;
        console.log(xhr.responseText);
    });
    console.log(param);
    xhr.send(param);
}

function excluirAluno() {
    var linha = event.target.closest("tr");
    var id = linha.querySelector('td').textContent;
    var page = "http://localhost/biblioteca/controller/deleteAluno.php?id=" + id;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", page);
    xhr.addEventListener("load", function () {
        var msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
            xhr.responseText +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button>'
        '</div>';
        document.querySelector('#msg').innerHTML = msg;
        consultarAlunos();
    });
    xhr.send();

}