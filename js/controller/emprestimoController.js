function cadastrarEmprestimo() {
    event.preventDefault();
    var codigo = document.querySelector('#codigo').value;
    var matricula = document.querySelector('#matricula').value;
    var param = "codigo=" + codigo + "&matricula=" + matricula;

    var page = "http://localhost/biblioteca/controller/createEmprestimo.php";

    var xhr = new XMLHttpRequest();
    xhr.open("POST", page);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.addEventListener("load", function () {
        var msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
            xhr.responseText +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button>'
        '</div>';
        document.querySelector('#msg').innerHTML = msg;
    });
    console.log(param);
    xhr.send(param);
}

function consultarEmprestimos(){
    event.preventDefault();
    var codigo = document.querySelector('#codigo').value;
    var matricula = document.querySelector('#matricula').value;

    var page = "http://localhost/biblioteca/controller/getEmprestimos.php";
    
    
    var cont = 0;
    page = page + '?'
    if (codigo.length){
        if(cont > 0){
            page = page + '&';
        }else{
            cont++;
        }
        page = page + 'codigo=' + codigo.replace(/ /g,"+");
    }
    if (matricula.length){
        if(cont > 0){
            page = page + '&';
        }else{
            cont++;
        }
        page = page + 'matricula=' + matricula.replace(/ /g,"+");
    }

    var xhr = new XMLHttpRequest();
    xhr.open("GET",  page );
    xhr.addEventListener("load", function(){
        document.querySelector("tbody").innerHTML = xhr.responseText;
        document.querySelector("table").hidden = false;
    });
    xhr.send();
}

function excluirEmprestimo() {
    var linha = event.target.closest("tr");
    var id = linha.querySelector('td').textContent;
    var page = "http://localhost/biblioteca/controller/deleteEmprestimo.php?id=" + id;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", page);
    xhr.addEventListener("load", function () {
        var msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
            xhr.responseText +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button>'
        '</div>';
        document.querySelector('#msg').innerHTML = msg;
        consultarEmprestimos();
    });
    xhr.send();

}