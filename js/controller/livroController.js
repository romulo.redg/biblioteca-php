function consultarLivros() {
    event.preventDefault();
    var titulo = document.querySelector('#titulo').value;
    var autor = document.querySelector('#autor').value;
    console.log(titulo + ' ' + autor);
    var page = "http://localhost/biblioteca/controller/getLivros.php";
    var cont = 0;
    page = page + '?'
    var tamanho = titulo.length;
    if (titulo.length) {
        if (cont > 0) {
            page = page + '&';
        } else {
            cont++;
        }
        page = page + 'titulo=' + titulo.replace(/ /g, "+");
    }
    if (autor.length) {
        if (cont > 0) {
            page = page + '&';
        } else {
            cont++;
        }
        page = page + 'autor=' + autor.replace(/ /g, "+");
    }
    console.log(page);

    var xhr = new XMLHttpRequest();
    xhr.open("GET", page);
    xhr.addEventListener("load", function () {
        document.querySelector("tbody").innerHTML = xhr.responseText;
        document.querySelector("table").hidden = false;
        console.log(xhr.responseText);
    });
    xhr.send();
}

function cadastrarLivro() {
    var titulo = document.querySelector("#titulo").value;
    var autor = document.querySelector("#autor").value;
    var param = "titulo=" + titulo + "&autor=" + autor;
    event.preventDefault();
    var page = "http://localhost/biblioteca/controller/createLivro.php";
    var xhr = new XMLHttpRequest();
    xhr.open("POST", page);
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.addEventListener("load", function () {
        var msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
            xhr.responseText +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button>'
        '</div>';
        document.querySelector('#msg').innerHTML = msg;
        console.log(xhr.responseText);
    });
    console.log(param);
    xhr.send(param);
}

function excluirLivro() {
    var linha = event.target.closest("tr");
    var codigo = linha.querySelector('td').textContent;
    var page = "http://localhost/biblioteca/controller/deleteLivro.php?id=" + codigo;
    var xhr = new XMLHttpRequest();
    xhr.open("GET", page);
    xhr.addEventListener("load", function () {
        var msg = '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
            xhr.responseText +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button>'
        '</div>';
        document.querySelector('#msg').innerHTML = msg;
        consultarLivros();
    });
    xhr.send();

}